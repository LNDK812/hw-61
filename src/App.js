import React, { Component, Fragment } from 'react';
import './App.css';

import axios from 'axios'
import CountryList from './components/CountryList/CountryList'
import CountryInfo from "./components/CountryInfo/CountryInfo";

class App extends Component {

  state = {
    countrylist : [],
    countryInfo : null,
  };


  componentDidMount() {
    const countryListUrl = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';
    const countryInfoUrl = 'https://restcountries.eu/rest/v2/alpha/';
    axios.get(countryListUrl).then(response => {
      console.log(response.data);
      this.setState({countrylist : response.data})
    })
  }

  getCountryInfo = (name) => {
    axios.get(`https://restcountries.eu/rest/v2/name/${name}`).then(response => {
      const country = response.data[0];
      Promise.all(country.borders.map(border => axios.get(`https://restcountries.eu/rest/v2/alpha/${border}`))).then(borderResponse => {
        country.borders = borderResponse.map(border => border.data.name);
        this.setState({
          countryInfo: country
        })
      })
    })
  };
  render() {
    console.log(this.state.countryInfo);
    return (
        <Fragment>
         <CountryList clicked={this.getCountryInfo} countrylist={this.state.countrylist}/>
        <CountryInfo info={this.state.countryInfo}/>
        </Fragment>
      )

  }
}
export default App;
