import React from 'react';
import './CountryList.css'
const CountryList = (props) => {
    return (
        <div className="block-list">
            {props.countrylist.map((country,id) => {
                return <div key={id} onClick={() => props.clicked(country.name)}><p>{country.name}</p></div>
            })}
        </div>
    );
};

export default CountryList;