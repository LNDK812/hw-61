import React from 'react';
import './CountryInfo.css'
const CountryInfo = (props) => {
    if(!props.info) return null
    return (
        <div className="info-block">
            <img src={props.info.flag} className='img' alt=""/>
            <p className='p'>{props.info.name}</p>
            <p>Capital : {props.info.capital}</p>
            <p>Population : {props.info.population}</p>
            Borders :
            <ul>
                {props.info.borders.map(border => {
                    return <li>{border}</li>
                })}
            </ul>
        </div>
    );
};

export default CountryInfo;